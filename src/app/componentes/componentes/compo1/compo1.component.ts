import { Component, OnInit } from '@angular/core';
import {  Sevicio1Service} from '../../../servicios/peticiones/sevicio1.service';
import { FormujsService } from 'src/app/servicios/peticiones/formujs.service';


@Component({
  selector: 'app-compo1',
  templateUrl: './compo1.component.html',
  styleUrls: ['./compo1.component.css']
})

export class Compo1Component implements OnInit {
  lado=4;
  listapersonal:any  [] = [{nombrePersonal:"",apellidoPersonal:"userSchema.apellidoPersonal",rolPersonal:"userSchema.rolPersonal",telefonoPersonal:"userSchema.telefonoPersonal",direccionPersonal:"userSchema.direccionPersonal",edadPersonal:'',id:"id"}]
 datos:any=[];
  mostrar = false;
  credentials = {nombrePersonal: ''};
  datosPersonal = {nombrePersonal: '', apellidoPersonal: '', rolPersonal: '', telefonoPersonal: '',direccionPersonal:'', edadPersonal: ''};
  id = '';
  _id = '';
  actualizar = {id: '', datosPersonal: ''};


  constructor(private _service:Sevicio1Service   ) {
   }
  ngOnInit(): void {
    this.consultaPersonal();
  }

  consultaPersonal(){
    this._service.consulta()
    .subscribe((resp:any) => {
      console.log(resp.respuesta);
      this.datos = resp.respuesta;
    })
  }
}

import { Injectable } from '@angular/core';
import{HttpClient} from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class Sevicio1Service {
  public url="http://localhost:3900";

    constructor(private _http:HttpClient){
      this.url
    }
    consultaper(){
      const url=`${this.url}/personal/:id`;
      return this._http.get(url);
      console.log(url);
    }
    consulta(){
      const url=`${this.url}/personal`;
      return this._http.get(url);
      console.log(url);
    }

    agregarper(body:any){
      const url=`${this.url}/personal/nuevo`;
      return this._http.post(url,body);
    }

    actualizarper(id:any,body:any){
      const url=`${this.url}/personal/actualizar/${id}`;
      return this._http.put(url,body);
    }
    eliminarper(id:any){
      const url=`${this.url}/personal/actualizar/${id}`;
      return this._http.delete(url);
    }


}

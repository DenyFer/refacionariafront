import { TestBed } from '@angular/core/testing';

import { Sevicio1Service } from './sevicio1.service';

describe('Sevicio1Service', () => {
  let service: Sevicio1Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(Sevicio1Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';

import { FormujsService } from './formujs.service';

describe('FormujsService', () => {
  let service: FormujsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FormujsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
